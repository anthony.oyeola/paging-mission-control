from unittest import TestCase

import mission_control
from mission_control import AlertQueue, ReadingLimit


class TestReadingLimit(TestCase):
    def test_high_low_values(self):
        reading_limit = ReadingLimit()
        self.assertIsNotNone(reading_limit)
        self.assertEqual(reading_limit.high, 1)
        self.assertEqual(reading_limit.low, 0)

    def test_reading_limit_in_order(self):
        reading_limit = ReadingLimit(low=2, high=4)
        self.assertLess(reading_limit.low, reading_limit.high)
        self.assertGreater(reading_limit.high, reading_limit.low)

    def test_reading_limit_out_of_order(self):
        with self.assertRaises(ValueError) as err:
            _ = ReadingLimit(low=6, high=4)
        exception = err.exception
        self.assertEqual(exception.args[1], '4 < 6')

    def test_reading_limit_same_value(self):
        with self.assertRaises(ValueError) as err:
            _ = ReadingLimit(low=5, high=5)
        exception = err.exception
        self.assertEqual(exception.args[1], '5 == 5')


class TestSatelliteRecord(TestCase):
    def setUp(self):
        data = "20180101 23:01:09.521|1000|10|8|4|2|1|BATT"
        self.record_with_alert = mission_control.create_telemetry_record(data)

        data = "20180101 23:01:09.521|1000|10|8|4|2|5|BATT"
        self.record_without_alert = mission_control.create_telemetry_record(data)

    def test_record_has_alert(self):
        self.assertTrue(self.record_with_alert.has_alert())

    def test_record_no_alert(self):
        self.assertFalse(self.record_without_alert.has_alert())

    def test_generate_alert(self):
        alert_obj = self.record_with_alert.generate_alert()
        self.assertIsNotNone(alert_obj)

        alert_obj = self.record_without_alert.generate_alert()
        self.assertIsNone(alert_obj)


class TestAlertQueue(TestCase):
    def setUp(self):
        self.alert_queue = AlertQueue(3)

        data = "20180101 23:01:09.521|1234|10|8|4|2|1|BATT"
        self.record = mission_control.create_telemetry_record(data)

    def test_enqueue_records(self):
        self.alert_queue.enqueue(self.record)

        alerts = self.alert_queue.battery_alerts
        self.assertGreater(len(alerts), 0)
        self.assertIn(self.record, alerts[self.record.sat_id])

    def test_dequeue_records(self):
        self.alert_queue.enqueue(self.record)
        old_record = self.alert_queue.dequeue(self.record)
        self.assertEqual(self.record, old_record)

        alerts = self.alert_queue.battery_alerts
        self.assertEqual(0, len(alerts[old_record.sat_id]))

    def test_alert_queue_time_window(self):
        alerts = self.alert_queue.battery_alerts
        for m in range(5):
            rec = mission_control.create_telemetry_record(f'20180101 23:0{m}:00.000|1234|10|8|4|2|1|BATT')
            self.alert_queue.enqueue(rec)
            self.assertLess(len(alerts[rec.sat_id]), 3)

    def test_report_json_creation(self):
        self.assertEqual(len(self.alert_queue.json_outputs), 0)

        for _ in range(3):
            self.alert_queue.enqueue(self.record)

        self.assertEqual(len(self.alert_queue.json_outputs), 1)
        json_obj = {
            "satelliteId": 1234,
            "severity": "RED LOW",
            "component": "BATT",
            "timestamp": "2018-01-01T23:01:09.521Z"
        }

        self.assertDictEqual(self.alert_queue.json_outputs[0], json_obj)
